// Bar Chart LaGomera pradzia

google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Menuo', 'Temperatūra dieną', 'Temperatūra Naktį'],
          ['Sausis', 21, 14],
          ['Vasaris', 21, 14],
          ['Kovas', 22, 15],
          ['Balandis', 22, 16],
          ['Gegužė', 23, 16],
          ['Birželis', 24, 18],
          ['Liepa', 26, 19],
          ['Rugpjūtis', 27, 21],
          ['Rugsėjis', 27, 21],
          ['Spalis', 26, 19],
          ['Lapkritis', 24, 18],
          ['Gruodis', 22, 16]
        ]);

        var options = {
          chart: {
           
          },
          bars: 'vertical',
          vAxis: {format: 'decimal'},
          height: 400,
          colors: ['#1b9e77', '#d95f02', '#7570b3']
        };

        var chart = new google.charts.Bar(document.getElementById('chart_div'));

        chart.draw(data, google.charts.Bar.convertOptions(options));

        var btns = document.getElementById('btn-group');

        btns.onclick = function (e) {

          if (e.target.tagName === 'BUTTON') {
            options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
            chart.draw(data, google.charts.Bar.convertOptions(options));
          }
        }
      }

      // Bar Chart pabaiga

      // Doughnut Chart LaGomera pradzia

            google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(gyvSkaicius);

      function gyvSkaicius() {

        var data = google.visualization.arrayToDataTable([
          ['Savivaldybė Tenerifėje', 'Gyventojų skaičius'],
[ " Adeje " , 25341 ] ,
[ " Arafo " , 5156  ] ,
[ " Arico " , 6653  ] ,
[ " Arona " , 52572 ] ,
[ " Buenavista del Norte  " , 5413  ] ,
[ " Candelaria  " , 15980 ] ,
[ " Fasnia  " , 2590  ] ,
[ " Garachico " , 5742  ] ,
[ " Granadilla de Abona " , 27244 ] ,
[ " La Guancha  " , 5294  ] ,
[ " Guía de Isora " , 16320 ] ,
[ " Güímar  " , 15920 ] ,
[ " Icod de los Vinos " , 21803 ] ,
[ " La Matanza de Acentejo  " , 7378  ] ,
[ " La Orotava  " , 39095 ] ,
[ " Puerto de la Cruz " , 30466 ] ,
[ " Los Realejos  " , 35299 ] ,
[ " El Rosario  " , 13718 ] ,
[ " San Cristóbal de La Laguna  " , 135004  ] ,
[ " San Juan de la Rambla " , 4938  ] ,
[ " San Miguel de Abona " , 9174  ] ,
[ " Santa Cruz de Tenerife  " , 217415  ] ,
[ " Santa Úrsula  " , 11571 ] ,
[ " Santiago del Teide  " , 10113 ] ,
[ " El Sauzal " , 8006  ] ,
[ " Los Silos " , 5426  ] ,
[ " Tacoronte " , 21442 ] ,
[ " El Tanque " , 3254  ] ,
[ " Tegueste  " , 9816  ] ,
[ " La Victoria de Acentejo " , 8152  ] ,
[ " Vilaflor  " , 1776  ] ,



        ]);

        var options = {
          pieHole: 0.5,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'Savivaldybė La Tenerifėje'
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single'));
        chart.draw(data, options);
      }
      //Doughnut Chart pabaiga


     