// Bar Chart LaGomera pradzia

google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Menuo', 'Temperatūra dieną', 'Temperatūra Naktį'],
          ['Sausis', 21, 15],
          ['Vasaris', 21, 15],
          ['Kovas', 22, 16],
          ['Balandis', 23, 16],
          ['Gegužė', 24, 17],
          ['Birželis', 26, 19],
          ['Liepa', 27, 21],
          ['Rugpjūtis', 28, 22],
          ['Rugsėjis', 28, 21],
          ['Spalis', 26, 20],
          ['Lapkritis', 24, 18],
          ['Gruodis', 22, 16]
        ]);

        var options = {
          chart: {
           
          },
          bars: 'vertical',
          vAxis: {format: 'decimal'},
          height: 400,
          colors: ['#1b9e77', '#d95f02', '#7570b3']
        };

        var chart = new google.charts.Bar(document.getElementById('chart_div'));

        chart.draw(data, google.charts.Bar.convertOptions(options));

        var btns = document.getElementById('btn-group');

        btns.onclick = function (e) {

          if (e.target.tagName === 'BUTTON') {
            options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
            chart.draw(data, google.charts.Bar.convertOptions(options));
          }
        }
      }

      // Bar Chart pabaiga

      // Doughnut Chart LaGomera pradzia

            google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(gyvSkaicius);

      function gyvSkaicius() {

        var data = google.visualization.arrayToDataTable([
          ['Savivaldybė La Gomeroje', 'Gyventojų skaičius'],
     [  " Antigua " , 11629 ] ,
[ " Betancuria  " , 811 ] ,
[ " La Oliva  " , 25083 ] ,
[ " Pájara  " , 20931 ] ,
[ " Puerto del Rosario  " , 36774 ] ,
[ " Tuineje " , 13946 ] ,


        ]);

        var options = {
          pieHole: 0.5,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'Savivaldybė La Gomeroje'
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single'));
        chart.draw(data, options);
      }
      //Doughnut Chart pabaiga