document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.carousel');
    var options = {fullWidth: true};
    var instances = M.Carousel.init(elems, options);
    var elems = document.querySelectorAll('.dropdown-trigger');
    var instances = M.Dropdown.init(elems);
  });

// Bar Chart LaGomera pradzia

google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Menuo', 'Temperatūra dieną', 'Temperatūra Naktį'],
          ['Sausis', 21, 14],
          ['Vasaris', 21, 14],
          ['Kovas', 22, 15],
          ['Balandis', 22, 16],
          ['Gegužė', 23, 16],
          ['Birželis', 24, 18],
          ['Liepa', 26, 19],
          ['Rugpjūtis', 27, 21],
          ['Rugsėjis', 27, 21],
          ['Spalis', 26, 19],
          ['Lapkritis', 24, 18],
          ['Gruodis', 22, 16]
        ]);

        var options = {
          chart: {
           
          },
          bars: 'vertical',
          vAxis: {format: 'decimal'},
          height: 400,
          colors: ['#1b9e77', '#d95f02', '#7570b3']
        };

        var chart = new google.charts.Bar(document.getElementById('chart_div'));

        chart.draw(data, google.charts.Bar.convertOptions(options));

        var btns = document.getElementById('btn-group');

        btns.onclick = function (e) {

          if (e.target.tagName === 'BUTTON') {
            options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
            chart.draw(data, google.charts.Bar.convertOptions(options));
          }
        }
      }

      // Bar Chart pabaiga

      // Doughnut Chart LaGomera pradzia

            google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(gyvSkaicius);

      function gyvSkaicius() {

        var data = google.visualization.arrayToDataTable([
          ['Savivaldybė La Gomeroje', 'Gyventojų skaičius'],
          ['Agulas',1180],
					['Alacheras',2048],
					['Hermigva',2183],
					['San Sebastian de la Gomera',9092],
					['Valjehermosas',3123],
					['Valje Gran Rėjus',	5150],

        ]);

        var options = {
          pieHole: 0.5,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'Savivaldybė La Gomeroje'
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single'));
        chart.draw(data, options);
      }
      //Doughnut Chart pabaiga


     