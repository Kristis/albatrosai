// Bar Chart LaGomera pradzia

google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Menuo', 'Temperatūra dieną', 'Temperatūra Naktį'],
          ['Sausis', 12, 7],
          ['Vasaris', 12, 6],
          ['Kovas', 12, 7],
          ['Balandis', 13, 7],
          ['Gegužė', 14, 8],
          ['Birželis', 15, 10],
          ['Liepa', 16, 12],
          ['Rugpjūtis', 17, 13],
          ['Rugsėjis', 18, 12],
          ['Spalis', 16, 11],
          ['Lapkritis', 14, 9],
          ['Gruodis', 13, 8]
        ]);

        var options = {
          chart: {
           
          },
          bars: 'vertical',
          vAxis: {format: 'decimal'},
          height: 400,
          colors: ['#1b9e77', '#d95f02', '#7570b3']
        };

        var chart = new google.charts.Bar(document.getElementById('chart_div'));

        chart.draw(data, google.charts.Bar.convertOptions(options));

        var btns = document.getElementById('btn-group');

        btns.onclick = function (e) {

          if (e.target.tagName === 'BUTTON') {
            options.vAxis.format = e.target.id === 'none' ? '' : e.target.id;
            chart.draw(data, google.charts.Bar.convertOptions(options));
          }
        }
      }

      // Bar Chart pabaiga

      // Doughnut Chart LaGomera pradzia

            google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(gyvSkaicius);

      function gyvSkaicius() {

        var data = google.visualization.arrayToDataTable([
          ['Savivaldybė La Gomeroje', 'Gyventojų skaičius'],
          ['Barlovento',2363],
          ['Breña Alta',7337],
          ['Breña Baja',5523],
          ['El Paso',7457],
          ['Fuencaliente de La Palma',1798],
          ['Garafía',1804],
          ['Los Llanos de Aridane', 20930],
          ['Puntagorda',2057],
          ['Puntallana',2346],
          ['San Andrés y Sauces',4473],
          ['Santa Cruz de la Palma',16330],
          ['Tazacorte',4911],
          ['Tijarafe',2776],
          ['Villa de Mazo',4858]

        ]);

        var options = {
          pieHole: 0.5,
          pieSliceTextStyle: {
            color: 'black',
          },
          legend: 'Savivaldybė La Gomeroje'
        };

        var chart = new google.visualization.PieChart(document.getElementById('donut_single'));
        chart.draw(data, options);
      }
      //Doughnut Chart pabaiga


     